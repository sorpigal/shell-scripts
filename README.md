# shell scripts

Shell scripts I've written.

* bio - a thin wrapper around dd that supports GNU style long options
* inotify-exec - run a command each time a file matches an inotify event
